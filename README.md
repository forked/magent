# magent

https://code.google.com/archive/p/memagent/

原名：memagent

File | Summary + Labels | Uploaded | Size
---- | ---------------- | -------- | ----
magent-0.6.tar.gz | memcached agent v0.6  `Type-Source`  `Featured` | Apr 15, 2010 | 16.85KB
magent-0.5.tar.gz | first stable release  `Deprecated`  `OpSys-Linux` | Sep 27, 2008 | 16.01KB
magent-0.4.tar.gz | magent 0.4  `Deprecated` | Sep 20, 2008 | 32.85KB
magent-0.3.tar.gz | backup servers farm/ketama algorithm added  `Deprecated` | Sep 16, 2008 | 31.49KB
memagent-0.2.tar.gz | initial working version 0.2 release  `Deprecated` | Sep 7, 2008 | 9.53KB

master，wiki 分支是 GitHub 上，别人从 google code 自动导出的，很多。
Automatically exported from code.google.com/p/memagent

---

simple but useful proxy program for memcached

**magent** is a simple but useful proxy program for [memcached](http://www.danga.com/memcached/) servers.

## It features:

- keeps connections to memcached servers
- supports following memcached commands
    1. get gets
    2. delete
    3. incr decr
    4. add set replace prepend append
    5. cas
- event-driven by using libevent library
- supports ketama algorithm
- backup servers farm
- unix domain socket

## Usage:

```
-h this message
-u uid
-g gid
-p port, default is 11211. (0 to disable tcp support)
-s ip:port, set memcached server ip and port
-b ip:port, set backup memcached server ip and port
-l ip, local bind ip address, default is 0.0.0.0
-n number, set max connections, default is 4096
-D do not go to background
-k use ketama key allocation algorithm
-f file, unix socket path to listen on. default is off
-i number, max keep alive connections for one memcached server, default is 20
-v verbose
```

## Changelog:

2010/4/14: memcached agent 0.6 1. add connection keepalive handler 1. bug fix, more robust, more debug messages

## Examples:

```sh
magent -s 10.1.2.1 -s 10.1.2.2:11211 -b 10.1.2.3:14000 -v
```

## Notes:

1. current magent's speed is about 80% of memcached's. finding way to speed it up(not good)
2. adding backup server does no harm to magent's performance. (good)
3. magent's cpu usage is less than memcached's and has very little memory usage. (good)
